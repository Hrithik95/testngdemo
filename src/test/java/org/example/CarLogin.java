package org.example;

import org.testng.annotations.*;

public class CarLogin {
    @Test
    public void webCarLogin(){
        System.out.println("WebCarLogin");
    }
    @Test
    public void mobileCarLogin(){
        System.out.println("mobileCarLogin");
    }
    @Test
    public void mobileCarSignIn(){
        System.out.println("mobileCarSignIn");
    }
    @Test(groups = {"Smoke"})
    public void mobileCarSignOut(){
        System.out.println("mobileCarSignnOut");
    }
    @BeforeSuite
    public void beforeCarSuite(){
        System.out.println("beforeCarSuite");
    }

    @Parameters({"URL"})
    @Test(dependsOnMethods = {"webCarLogin"}) //use it while running testng.xml file
    public void apiCarLogin(String urlName){
        System.out.println("APILogin");
//        System.out.println("car: "+urlName);
    }

//    @Test(dependsOnMethods = {"webCarLogin"}) //use it while doing regression testing
//    public void apiCarLogin(){
//        System.out.println("APILogin");
//    }
    @BeforeTest
    public void firstTest(){
        System.out.println("first test to be executed in this car test folder");
    }
    @AfterSuite
    public void afterSuiteForCarLoan(){
        System.out.println("afterSuiteForCarLoan");
    }
}
