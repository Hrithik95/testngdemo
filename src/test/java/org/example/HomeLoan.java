package org.example;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class HomeLoan {
    @Test(groups = {"Smoke"})
    public void webHomeLoan(){
        System.out.println("WebHomeLoan");
    }
    @Test(timeOut = 4000)
    public void mobileHomeLoan(){
        System.out.println("mobileHomeLoan");
    }
    @Parameters({"URL","personal-api"})
    @Test(dataProvider = "getData",dataProviderClass = HomeLoan.class)
    public void apiHomeLoan(String urlName, String api){
        System.out.println("APIHomeLoan");
        System.out.println("home: "+urlName);
        System.out.println("home-api: "+api);
    }
    @BeforeSuite
    public void beforeHomeSuite(){
        System.out.println("beforeHomeSuite");
    }
    @DataProvider
    public Object[][] getData(){
        Object[][] data = new Object[3][2];//3 data-sets with two data columns in each data-set
        data[0][0]="firstusername";
        data[0][1]="firstpassword";
        data[1][0]="secondusername";
        data[1][1]="secondpassword";
        data[2][0]="thirdusername";
        data[2][1]="thirdpassword";
        return data;
    }
}
