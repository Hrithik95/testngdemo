package org.example;

import org.testng.annotations.*;

public class day1 {
    @AfterTest
    public void lastExecution(){
        System.out.println("I will be executed at the last of this test folder");
    }
    @BeforeMethod
    public void beforeEveryMethod(){
        System.out.println("before every method runs in day-1");
    }
    @AfterMethod
    public void afterEveryMethod(){
        System.out.println("after every method runs in day-1");
    }
    @Test(groups = {"Smoke"})
    public void demo1(){
        System.out.println("demo1");
    }
@Parameters({"URL"})
    @Test
    public void demo2(String urlName){
        System.out.println("demo2");
//        System.out.println(urlName);
    }
    //use below method while running regressiontesting and above method while using testng.xml file
//    @Test
//    public void demo2(){
//        System.out.println("demo2");
//    }
    @AfterSuite
    public void afterSuiteForDay1(){
        System.out.println("afterSuiteForDay1");
    }
}
