package org.example;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class day2 {
    @Test(groups = {"Smoke"},enabled = false)
    public void demo3(){
        System.out.println("demo3");
    }
    @BeforeTest
    public void preRequisites(){
        System.out.println("I will be executed first");
    }
    @BeforeClass
    public void beforeClassDay2(){
        System.out.println("before class day-2");
    }
    @AfterClass
    public void afterClassDay2(){
        System.out.println("after class day-2");
    }
}
